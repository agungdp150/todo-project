import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom" 
import {Provider} from "react-redux"
import store from "./store"

// Import Components
  import Home from "./components/Home.js"
  import RegisterForm from "./components/RegisterForm.js"
  import AfterRegister from "./components/AfterRegister.js"
  import ForgotPassword from "./components/ForgotPassword.js"

// Import Style
import './App.css';

class App extends Component {
  render () {
  return (
    <Provider store={store}>
      <Router>
    <div className="App">
      <Switch>
          <Route path="/" exact component = {Home}/>
          <Route path="/regis" exact component = {RegisterForm}/>
          <Route path="/forgot" exact component ={ForgotPassword}/>
          <Route parh = "/login/:user" exact component = {AfterRegister}/>
      </Switch>
    </div>
    </Router>
    </Provider>
  );
  }
}

export default App;
