import {combineReducers} from "redux";
import loginReducer from "./loginReducer"
import totodoReducer from "./totodoReducer"

export default combineReducers ({
  loginState : loginReducer,
  loginTodo : totodoReducer
});