import axios from "axios";

const setToken = token => {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
  } else {
    // axios.delete.headers.common["Authorization"] = null
    delete axios.headers.common["Authorization"]
  }
}

export default setToken;