import React, { Component } from "react";
import { Link } from "react-router-dom";

class ForgotPassword extends Component {
  render() {
    return (
      <div className='re-container'>
        <div className='re-font'>
          <h1><i className='fas fa-key fa-3x'/>?</h1>
          <h3>Yo! Forgot Password ?</h3>
          <p>No Worries! Enter your email and we send you a reset.</p>
        </div>
        <div className='re-form'>
          <form>
            <input 
              type='email' 
              required 
              placeholder='todo@email.com'
              />
            <br />
            <button>Send Request</button>
          </form>
        </div>
        <Link to='/'>
          Back to Login...
        </Link>
      </div>
    );
  }
}

export default ForgotPassword;
