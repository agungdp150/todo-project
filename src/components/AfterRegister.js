import React, { Component } from 'react'
import {Link} from "react-router-dom"
import axios from "axios"

class AfterRegister extends Component {
  constructor (props) {
    super (props);

    this.state = {
      listData : [],
      name : "",
      description : "",
      studio : ""
    }
  }
  
  // Get Data from API
  getData = async() => {
    await  axios ({
      method : "GET",
      url : `https://glints-academy.herokuapp.com/api/v1/anime`,
      headers : {
        "Content-Type" : "application/json"
    }    
  })
  .then(response => {
    this.setState({listData : response.data.animes})
  })
}

componentDidMount = async () => {
  await this.getData()
}


  // Change nilai state
  handleChange = (e) => {
    this.setState({
      [e.target.name] : e.target.value
    })
  }

  // Menambahkan Data to API
  handleInput = async (e) => {
    e.preventDefault()
    // console.log("ok")

    await axios.post(`https://glints-academy.herokuapp.com/api/v1/anime`, this.state)
    .then(response => {
      console.log (response)
    })
    .catch (error => {
      console.log (error)
    })

    // await axios ({
    //   method : "POST",
    //   url : `https://glints-academy.herokuapp.com/api/v1/anime`,
    //   headers : {
    //     "Content-Type" : "application/json"
    //   },
    //   body : this.state
    // })
    // .then (response => console.log (response))
    // .catch(err => console.log (`this my error : ${err}`))
  }

  // Handle Delete
  handleDelete (id) {
    if (window.confirm("Are you sure?")) {
      fetch(`https://glints-academy.herokuapp.com/api/v1/anime/${id}`, {
        method : "delete",
        header :{
          "Content-Type" : "application/json"
        }
      })
    }
  }

  render () {
    const {name, description, studio} = this.state

    const listNow = this.state.listData.map (dataList => {
      return (
        <div key={dataList._id}>
          <h2>{dataList.name}</h2>
          <h4>{dataList.studio}</h4>
          <p>{dataList.description}</p>
          <hr/>
          <button
            onClick={() => this.handleDelete(dataList._id)}
            variant = "danger"
            >Delete</button>
          <button>
            Edit
            </button>
        </div>
      )
    })
    return (
      <div className="af-container">
        <nav >
          <div className="logo">
          <h4>TOdo</h4>
          </div>
          <ul className="nav-links">
          <Link to="/">
          <li>Sign Out</li>
          </Link>
          </ul>
        </nav>
        <form onSubmit={this.handleInput} className="af-input">
          <h3>What your list?</h3>
          <input
            type = "text"
            placeholder="name"
            name = "name"
            autoComplete = "off"
            value = {name}
            onChange = {this.handleChange}
          /><br/>
          <input
            type = "text"
            placeholder="studio"
            autoComplete = "off"
            name = "studio"
            value = {studio}
            onChange = {this.handleChange}
          /><br/>
          <textarea
            type = "text"
            placeholder = "description"
            name = "description"
            value = {description}
            onChange = {this.handleChange}
          />
          <br/>
          <button type="submit">+</button>
        </form>
        <div className="list-data">
          {listNow}
        </div>
        <div className="ket-plus">
        <hr/>
          &copy; 2019 Copyright TOdo Project
      </div>
      </div>
    )
  }
}

export default AfterRegister;
