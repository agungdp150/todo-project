import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { register } from "../actions/loginAction";

class RegisterForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      password: ""
    };
  }

  // handle change
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  // Handle input submit

  handleSubmit = e => {
    e.preventDefault();
    console.log("oke");

    const formData = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };
    // console.log (formData)
    this.props.register(formData);
    this.props.history.push("./login/:user");
  };

  render() {
    const { name, email, password } = this.state;
    return (
      <div>
        <div className='in-sign'>
          <i className='fas fa-chevron-down fa-6x' />
          <h1>Welcome to List App</h1>
          <p>Create your account now and making your list To Do</p>
        </div>
        <div className='f-container'>
          <h1> Sign Up to To Do App </h1>
          <div className='f-icon'>
            <p><i className='fab fa-facebook-square fa-2x' /></p>
            <p><i className='fab fa-google-plus-square fa-2x' /></p>
            <p><i className='fab fa-linkedin fa-2x' /></p>
          </div>
          <p className='p-con1'> login using your account </p>
          <div className='f-form1'>
            <form onSubmit={this.handleSubmit}>
              <input
                key={1}
                type='text'
                required
                placeholder='Demond Reichel'
                name='name'
                value={name}
                onChange={this.handleChange}
              />
              <input
                key={2}
                type='email'
                required
                placeholder='todo@todomail.com'
                name='email'
                value={email}
                onChange={this.handleChange}
              />
              <input
                key={3}
                type='password'
                required
                placeholder='password'
                minLength='8'
                name='password'
                value={password}
                onChange={this.handleChange}
              />
              <div className='btn-name'>
                <button type='submit'>
                  Sign Up
                </button>
                <div className='btn-re re-1'>
                  <Link to='/'>
                      Login 
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  // console.log (state)
  return {
    isAuthenticated: state.loginState.isAuthenticated
  };
};

export default connect(
  mapStateToProps,
  { register }
)(RegisterForm);
