import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { login } from "../actions/loginTo.js";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
    };
  }

  // handle change
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  // Handle input submit

  handleSubmit = e => {
    e.preventDefault();
    console.log("oke");

    const formData = {
      email: this.state.email,
      password: this.state.password
    };
    // console.log (formData)
    this.props.login(formData);
    this.props.history.push("./login/:user");
  };

  render() {
    const {email, password} = this.state

    return (
      <div>
        <div className='in-sign'>
          <i className='fas fa-chevron-down fa-6x' />
          <h1>Welcome to List App</h1>
          <p>To Do List App to simplify your task management everyday</p>
        </div>
        <div className='f-container'>
          <h1>Sign In to To Do App</h1>
          <div className='f-icon'>
            <p><i className='fab fa-facebook-square fa-2x' /></p>
            <p><i className='fab fa-google-plus-square fa-2x' /></p>
            <p><i className='fab fa-linkedin fa-2x' /></p>
          </div>
          <p className='p-con1'>login using your account</p>
          <div className='f-form1'>
            <form onSubmit={this.handleSubmit}>
              <input 
              type='email' 
              placeholder='todo@email.com' 
              name = "email"
              value = {email}
              onChange = {this.handleChange}
              />
              <br />
              <input
                type='password'
                required
                placeholder='Password'
                minLength='8'
                name = "password"
                value = {password}
                onChange = {this.handleChange}
              />
              <br />
              <div className='btn-name'>
                <button type="submit">Sign In</button>
                <div className='btn-re'>
                  <Link to='/forgot'>
                    <p>Forgot Password ?</p>
                  </Link>
                  <p>didn't have account yet? register</p>
                  <Link to='/regis'>
                    <p>
                      <span>here...</span>
                    </p>
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => {
  // console.log (state)
  return {
    isAuthenticated: state.loginTodo.isAuthenticated
  };
};

export default connect(
  mapStateToProps,
  { login }
)(Home);
