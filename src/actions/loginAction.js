import axios from "axios";
import {REGISTER_SUCCESS, REGISTER_FAIL} from "../types"
// import RegisterForm from "../components/RegisterForm";
// import { dispatch } from "rxjs/internal/observable/range";

export const register = RegisterForm => async dispatch => {
  try {
    const response = await axios.post (
      "https://ga-todolist-api.herokuapp.com/api/user", RegisterForm
    );
    console.log (response.data);
    dispatch ({
      type : REGISTER_SUCCESS,
      payload : response.data
    });
  } catch(error) {
    console.log (error.response.data);
    dispatch ({
      type : REGISTER_FAIL
    })
  }
}


// try {
// await axios ({
//   method : "POST",
//   url : "https://glints-academy.herokuapp.com/api/v1/users",
//   headers : {
//     "Content-Type" : "application/json"
//     }
//   }).catch (err => console.log (err.response))
// }