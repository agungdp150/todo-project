import axios from "axios";
import {REGISTER_SUCCESS, REGISTER_FAIL} from "../types"


export const login = Home => async dispatch => {
  try {
    const response = await axios.post (
      "https://ga-todolist-api.herokuapp.com/api/user/login", Home
    );
    console.log (response.data);
    dispatch ({
      type : REGISTER_SUCCESS,
      payload : response.data
    });
  } catch(error) {
    console.log (error.response.data);
    dispatch ({
      type : REGISTER_FAIL
    })
  }
}